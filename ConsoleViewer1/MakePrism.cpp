#include "Viewer.h"
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <gp_Pln.hxx>
#include <Geom_Plane.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>
#include <BRepFilletAPI_MakeFillet.hxx>
#include <TopoDS_Iterator.hxx>

class MakePrism
{
public:

    MakePrism(const double L, const double W, const double H) : Length(L), Width(W), Height(H)
    {
        this->build();
    }

public:

    const TopoDS_Shape& GetResult() const
    {
        return m_result;
    }

    operator TopoDS_Shape()
    {
        return m_result;
    }

private:

    void build()
    {
        gp_Pnt2d ContourArray[4] = {
            gp_Pnt2d(0, 0),
            gp_Pnt2d(Length, 0),
            gp_Pnt2d(Length, Width),
            gp_Pnt2d(0, Width) };

        // P = S(u,v)
        Handle(Geom_Plane) plane = new Geom_Plane(gp_Pln(gp::XOY()));

        gp_Pnt pointsOnPlane[4];
        int i = 0;
        //
        for (auto point_uv : ContourArray)
            pointsOnPlane[i++] = plane->Value(point_uv.X(), point_uv.Y());

        std::vector<TopoDS_Edge> edges;
        edges.push_back( BRepBuilderAPI_MakeEdge(pointsOnPlane[0], pointsOnPlane[1]));
        edges.push_back(BRepBuilderAPI_MakeEdge(pointsOnPlane[1], pointsOnPlane[2]));
        edges.push_back(BRepBuilderAPI_MakeEdge(pointsOnPlane[2], pointsOnPlane[3]));
        edges.push_back(BRepBuilderAPI_MakeEdge(pointsOnPlane[3], pointsOnPlane[0]));

        //---------------------
        BRepBuilderAPI_MakeWire mkWire;
        for (auto edge : edges)
            mkWire.Add(edge);
        //-------------


        //-------------
        const TopoDS_Wire& wire = mkWire.Wire();
        TopoDS_Shape wireShape = BRepBuilderAPI_MakeFace(wire);
        //-------------

        //-------------
        BRepPrimAPI_MakePrism makePrism(wireShape, gp_Vec(plane->Axis().Direction()) * Height);
        TopoDS_Shape prismShape = makePrism.Shape();
        //-------------

        // Get vertixes
        //-------------
        TopTools_IndexedMapOfShape vertixes;
        TopExp::MapShapes(wireShape, TopAbs_VERTEX, vertixes);
        //-------------


        TopoDS_Compound edges2Blend; // TShape (null)
        BRep_Builder bbuilder;
        bbuilder.MakeCompound(edges2Blend); // TShape (not null)

        for (int i = 1; i <= vertixes.Extent(); ++i)
        {
            const TopoDS_Shape& vertix = vertixes(i);

            // V -> Prism -> V1 -> BOP -> V2
            //        H1            H2
            // H = H1.Merge(H2)...
            //
            // H(V) -> V2

            const TopTools_ListOfShape& shapeVertixes = makePrism.Generated(vertix);
            

            for (TopTools_ListOfShape::Iterator it(shapeVertixes); it.More(); it.Next())
            {
                const TopoDS_Shape& image = it.Value();

                if (image.ShapeType() == TopAbs_EDGE)
                {
                    const TopoDS_Edge& edge = TopoDS::Edge(image);
                    bbuilder.Add(edges2Blend, edge);
                }
            }
        }

        BRepFilletAPI_MakeFillet mkFillet(prismShape);
        //
        for (TopoDS_Iterator it(edges2Blend); it.More(); it.Next())
        {
            const TopoDS_Edge& edge = TopoDS::Edge(it.Value());

            mkFillet.Add(1., edge);
        }
        //
        mkFillet.Build();

        m_result = mkFillet.Shape();
    }

private:

    double       Length, Width, Height;
    TopoDS_Shape m_result;

};
