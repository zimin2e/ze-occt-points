// Local includes
#include "Viewer.h"
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <gp_Pln.hxx>
#include <Geom_Plane.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>
#include <BRepFilletAPI_MakeFillet.hxx>
#include <TopoDS_Iterator.hxx>
#include <STEPControl_Writer.hxx>
#include "Viewer.h"
#include <IGESControl_Reader.hxx>
#include <BOPAlgo_Builder.hxx>
#include <BOPAlgo_PaveFiller.hxx>
#include <TopExp_Explorer.hxx>
#include <BRep_Builder.hxx>
#include <TopoDS.hxx>
#include <ShapeAnalysis_Edge.hxx>
#include <ShapeAnalysis_Curve.hxx>


//-----------------------------------------------------------------------------
bool LoadIGES(const TCollection_AsciiString& filename,
    TopoDS_Shape& result)
{
    IGESControl_Reader reader;
    IFSelect_ReturnStatus outcome = reader.ReadFile(filename.ToCString());
    //
    if (outcome != IFSelect_RetDone)
        return false;

    reader.TransferRoots();

    result = reader.OneShape();
    return true;
}
#include <gp_Ax2.hxx>
#include <TopoDS_Shape.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <STEPControl_Writer.hxx>
#include <StlAPI_Writer.hxx>
#include <VrmlAPI_Writer.hxx>

//-----------------------------------------------------------------------------

#include "MakePrism.cpp";
#include "Wire.cpp";
#include "Cilinder.cpp";

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    PSTR szCmdLine, int iCmdShow)
{
  // Init Viewer
 //--------------------
  Viewer viewer(50, 50, 500, 500);

  //--------------------
  Cilinder cilinder = Cilinder();
  cilinder.Make(viewer);
  
  //--------------------
  const double L = 20;
  const double W = 10;
  const double H = 5;
  TopoDS_Shape prism = MakePrism(L, W, H);
  viewer << prism;

  //--------------------
  Wire wire = Wire();
  wire.Make(viewer);

  
  // Show Viewer
  //--------------------
  viewer.StartMessageLoop();

  return 0;
}
