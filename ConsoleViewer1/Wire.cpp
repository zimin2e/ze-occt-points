#include "Viewer.h"
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <gp_Pln.hxx>
#include <Geom_Plane.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Wire.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>
#include <BRepFilletAPI_MakeFillet.hxx>
#include <TopoDS_Iterator.hxx>
#include <gp_Ax2.hxx>
#include <TopoDS_Shape.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <STEPControl_Writer.hxx>
#include <StlAPI_Writer.hxx>
#include <VrmlAPI_Writer.hxx>

//-----------------------------------------------------------------------------
#include <gp_Circ.hxx>
#include <TopoDS_Edge.hxx>
#include <BRepBuilderAPI_MakeEdge.hxx>
#include <BRepMesh_IncrementalMesh.hxx>
#include <STEPControl_Writer.hxx>
//#include <StlAPI_Writer.hxx>
#include <VrmlAPI_Writer.hxx>
#include <GC_MakeArcOfCircle.hxx>
#include <GC_MakeSegment.hxx>
#include <TopoDS_Wire.hxx>
#include <BRepBuilderAPI_MakeWire.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <BRepBuilderAPI_MakeFace.hxx>
#include <BRepPrimAPI_MakePrism.hxx>
#include <BRepFilletAPI_MakeFillet.hxx>
#include <BRepOffsetAPI_MakeThickSolid.hxx>
#include <TopExp_Explorer.hxx>

class Wire
{
public:
	TopoDS_Shape Make(Viewer& viewer)
	{
		TopoDS_Shape result;

        // Create primitives
        // Cylinder
        gp_Ax2 anAxis;
        anAxis.SetLocation(gp_Pnt(0.0, 30.0, 0.0));
        TopoDS_Shape aCylinder = BRepPrimAPI_MakeCylinder(anAxis, 3.0, 5.0).Shape();
        BRepMesh_IncrementalMesh(aCylinder, 0.1, Standard_True);
        viewer << aCylinder;

        // Circle
        anAxis.SetLocation(gp_Pnt(16.0, 60.0, 0.0));
        TopoDS_Edge aCircle = BRepBuilderAPI_MakeEdge(gp_Circ(anAxis, 3.0));
        BRepMesh_IncrementalMesh(aCircle, 0.1, Standard_True, 0.1);
        viewer << aCircle;

        Standard_Real myWidth1 = 40;
        Standard_Real myHeight1 = 50;
        gp_Pnt example_point1(myWidth1 / 2., -myHeight1 / 4., 0);
        gp_Pnt example_point2(myWidth1 / 2., 0, 0);
        Handle(Geom_TrimmedCurve) aSegment112 = GC_MakeSegment(example_point1, example_point2);
        TopoDS_Edge anEdge112 = BRepBuilderAPI_MakeEdge(aSegment112);
        viewer << anEdge112;

        //----------------
        //TopoDS_Shape bottle = MakeBottle(100., 100., 100.);
        //viewer << bottle;

        //--------------------
        Standard_Real myWidth = 100;
        Standard_Real myHeight = 100;
        Standard_Real myThickness = 100;

        // Profile : Define Support Points
        gp_Pnt aPnt1(-myWidth / 2., 0, 0);
        gp_Pnt aPnt2(-myWidth / 2., -myThickness / 4., 0);
        gp_Pnt aPnt3(0, -myThickness / 2., 0);
        gp_Pnt aPnt4(myWidth / 2., -myThickness / 4., 0);
        gp_Pnt aPnt5(myWidth / 2., 0, 0);
        // Profile : Define the Geometry
        Handle(Geom_TrimmedCurve) anArcOfCircle = GC_MakeArcOfCircle(aPnt2, aPnt3, aPnt4);
        Handle(Geom_TrimmedCurve) aSegment1 = GC_MakeSegment(aPnt1, aPnt2);
        Handle(Geom_TrimmedCurve) aSegment2 = GC_MakeSegment(aPnt4, aPnt5);
        // Profile : Define the Topology
        TopoDS_Edge anEdge1 = BRepBuilderAPI_MakeEdge(aSegment1);
        TopoDS_Edge anEdge2 = BRepBuilderAPI_MakeEdge(anArcOfCircle);
        TopoDS_Edge anEdge3 = BRepBuilderAPI_MakeEdge(aSegment2);
        TopoDS_Wire aWire = BRepBuilderAPI_MakeWire(anEdge1, anEdge2, anEdge3);
        viewer << aWire;
		return result;
	}
};
