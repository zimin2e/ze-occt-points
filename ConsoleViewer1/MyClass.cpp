#include "Viewer.h"

class MyClass : public Standard_Transient
{
    DEFINE_STANDARD_RTTI_INLINE(MyClass, Standard_Transient)

public:

    MyClass() : Standard_Transient()
    {
        std::cout << "ctor" << std::endl;
    }

    virtual ~MyClass()
    {
        std::cout << "dtor" << std::endl;
    }

    void DoSmth()
    {
        std::cout << "DoSmth()" << std::endl;
    }
};

void Foo(const Handle(MyClass)& arg)
{
    arg->DoSmth();
}
